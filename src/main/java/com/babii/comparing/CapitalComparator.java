package com.babii.comparing;

import java.util.Comparator;

public class CapitalComparator implements Comparator<Country> {
    @Override
    public int compare(Country first, Country second) {
        return first.getCapitalName().compareToIgnoreCase(second.getCapitalName());
    }
}
