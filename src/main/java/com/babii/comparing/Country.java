package com.babii.comparing;

public class Country implements Comparable<Country> {
    private String countryName;
    private String capitalName;

    public Country(String countryName, String capitalName) {
        super();
        this.countryName = countryName;
        this.capitalName = capitalName;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCapitalName() {
        return capitalName;
    }

    @Override
    public int compareTo(Country c) {
        return countryName.compareToIgnoreCase(c.getCountryName());
    }

    @Override
    public String toString() {
        return "\n" + countryName + " - " + capitalName;
    }
}
