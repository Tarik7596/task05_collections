package com.babii.comparing;

import java.util.*;

public class Main {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static Country[] generateCountries(int count) {
        String[] countries = { "Ukraine", "Russia", "Moldova", "Hungary", "Poland", "Belarus", "italy" };
        String[] capitals = { "Kyiv", "Moscow", "Kyshyniv", "Budapest", "Warzsaw", "Minsk", "roma" };

        Country[] result = new Country[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            int index = random.nextInt(countries.length);
            result[i] = new Country(countries[index], capitals[index]);
        }
        return result;
    }

    public static void main(String[] args) {
        List<Country> list = null;
        Country[] array = null;

        while (true) {
            System.out.println("------------------------------");
            System.out.println("1 - заповнити ліст рандомними даними");
            System.out.println("2 - заповнити масив рандомними даними");
            System.out.println("3 - вивести на екран ліст");
            System.out.println("4 - вивести на екран масив");
            System.out.println("5 - посортувати ліст за назвою країни");
            System.out.println("6 - посортувати масив за назвою країни");

            System.out.println("7 - посортувати ліст за назвою столиці");
            System.out.println("8 - посортувати масив за назвою столиці");

            System.out.println("9 - бінарний пошук по лісту");
            System.out.println("10 - бінарний пошук по масиву");
            System.out.println("------------------------------");
            int choose = 0;
            choose = SCANNER.nextInt();

            switch (choose) {
                case 1: {
                    System.out.print("кількість=");
                    int count = SCANNER.nextInt();
                    list = new ArrayList<Country>(Arrays.asList(generateCountries(count)));
                    break;
                }
                case 2: {
                    System.out.print("кількість=");
                    int count = SCANNER.nextInt();
                    array = generateCountries(count);
                    break;
                }
                case 3:
                    System.out.println(list);
                    break;
                case 4:
                    System.out.println(Arrays.toString(array));
                    break;
                case 5:
                    list.sort(null);
                    break;
                case 6:
                    Arrays.sort(array);
                    break;
                case 7:
                    list.sort(new CapitalComparator());
                    break;
                case 8:
                    Arrays.sort(array, new CapitalComparator());
                    break;
                case 9:
                {
                    System.out.print("Введіть столицю: ");
                    String capital = SCANNER.next();
                    int index = Collections.binarySearch(list,
                            new Country("", capital),
                            new CapitalComparator());
                    System.out.println("index = " + index);
                    break;
                }
                case 10:
                {
                    System.out.print("Введіть столицю: ");
                    String capital = SCANNER.next();
                    int index = Arrays.binarySearch(array,
                            new Country("", capital),
                            new CapitalComparator());

                    System.out.println("index = " + index);
                    break;
                }
                case 0:
                    System.out.println("exit...");
                    System.exit(0);
                default:
            }
        }
    }
}
