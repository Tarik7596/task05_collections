package com.babii.mydeque;

public class Main {
    public static void main(String[] args) {
        MyDeque myDeque = new MyDeque();
        System.out.println(myDeque);
        myDeque.addFirst(4);
        myDeque.addLast(6);
        myDeque.addFirst(10);
        System.out.println("DEQUE: " + myDeque);
        myDeque.removeFirst();
        myDeque.removeLast();
        System.out.println("DEQUE: " + myDeque);
    }
}
