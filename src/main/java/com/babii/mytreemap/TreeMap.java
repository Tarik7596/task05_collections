package com.babii.mytreemap;

public class TreeMap {
    private class Node {
        public Integer key;
        public String value;
        public Node left;
        public Node right;

        public Node(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }
    private Node root;
    public void put(Integer key, String value) {
        Node newNode = new Node(key, value);
        if (root == null) {
            root = newNode;
        } else {
            Node current = root;
            while (true) {
                if (newNode.key == current.key) {
                    current.value = value;
                    break;
                }
                if (newNode.key > current.key) {
                    if (current.right == null) {
                        current.right = newNode;
                        break;
                    }
                    current = current.right;
                } else {
                    if (current.left == null) {
                        current.left = newNode;
                        break;
                    }
                    current = current.left;
                }
            }
        }
    }
    public String remove(Integer key) {
        Node current = root;
        Node currentParent = null;

        while (current != null && current.key != key) {
            currentParent = current;
            if (key > current.key)
                current = current.right;
            else
                current = current.left;
        }
        if (current != null) {
            Node removedNode = null;

            //якщо вузол має лише одного нащадка або не має зовсім нащадків
            if (current.left == null || current.left == null) {
                removedNode = current;
                Node removedNodeChild = null;
                if (current.left != null)
                    removedNodeChild = current.left;
                else if (current.right != null)
                    removedNodeChild = current.right;
                if (currentParent == null)
                    root = removedNodeChild;
                else {
                    if (currentParent.left == current)
                        currentParent.left = removedNodeChild;
                    else
                        currentParent.right = removedNodeChild;
                }
                // інакше шукаємо найбільш лівий нащадок правої дитини
                // нашого вузла, і присвоюємо нашому вузлу значення ключа цього
                // найлівішого вузла, а після цього видаляємо цей найлівіший вузол
            } else {
                Node mostLeft = current.right;
                Node mostLeftParent = current;
                while (mostLeft.left != null) {
                    mostLeftParent = mostLeft;
                    mostLeft = mostLeft.left;
                }
                current.key = mostLeft.key;
                // value is not assigning
                removedNode = mostLeft;

                if (mostLeftParent.left == mostLeft)
                    mostLeftParent.left = null;
                else
                    mostLeftParent.right = null;
            }
            String value = new String(removedNode.value);
            removedNode = null;
            return value;
        }
        return null;
    }

    public String get(Integer key) {
        Node current = root;
        while (current != null) {
            if (key > current.key)
                current = current.right;
            else if (key < current.key)
                current = current.left;
            else
                return current.value;
        }
        return null;
    }
    // вивід дерева на екран в повернутому
    // на 90 градусів проти год стрілки вигляді
    private void recursivePrint(Node cur, int level) {
        if (cur != null) {
            recursivePrint(cur.right, level + 1);
            if (level == 0) {
                System.out.print("root-> ");
                for (int i = 0; i < level; i++)
                    System.out.print("   ");
            } else {
                for (int i = 0; i < level + 2; i++)
                    System.out.print("    ");
            }
            System.out.println(cur.key + "(" + cur.value + ")");
            recursivePrint(cur.left, level + 1);
        }
    }
    public void print() {
        recursivePrint(root, 0);
    }
}
