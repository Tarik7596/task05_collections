package com.babii.mytreemap;

import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        TreeMap treeMap = new TreeMap();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("---------------------------------------");
            System.out.println("1-put, 2-remove, 3-print, 4-get, 0-exit");
            System.out.println("----------------------------------------");
            int choose;
            try {
                choose = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Incorrect input");
                scanner.next();
                continue;
            }
            switch (choose) {
                case 1:
                    TreeCommand.PUT.execute(treeMap);
                    break;
                case 2:
                    TreeCommand.REMOVE.execute(treeMap);
                    break;
                case 3:
                    TreeCommand.PRINT.execute(treeMap);
                    break;
                case 4:
                    TreeCommand.GET.execute(treeMap);
                    break;
                case 0:
                    TreeCommand.EXIT.execute(treeMap);
                    scanner.close();
                    break;
                default:
                    System.out.println("Incorrect input");
            }
        }
    }
}
