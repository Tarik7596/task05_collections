package com.babii.mytreemap;

import java.util.Scanner;

public enum TreeCommand {
    PUT {
        public void execute(TreeMap tm) {
            System.out.print("key = ");
            int key = scanner.nextInt();
            System.out.print("value = ");
            String value = scanner.next();
            tm.put(key, value);
        }
    },
    REMOVE {
        public void execute(TreeMap tm) {
            System.out.print("key = ");
            int key = scanner.nextInt();
            if (tm.get(key) == null) {
                System.out.println("cant remove it, an element with the key doesn't exist");
            } else
                tm.remove(key);
        }
    },
    PRINT {
        public void execute(TreeMap tm) {
            tm.print();
        }
    },
    GET {
        public void execute(TreeMap tm) {
            System.out.print("key = ");
            int key = scanner.nextInt();
            String value = tm.get(key);
            if (value != null) {
                System.out.println("value = " + value);
            } else
                System.out.println("cant find an element with the key");
        }
    },
    EXIT {
        public void execute(TreeMap tm) {
            System.exit(0);
        }
    };
    Scanner scanner = new Scanner(System.in);
    public abstract void execute(TreeMap tm);
}
