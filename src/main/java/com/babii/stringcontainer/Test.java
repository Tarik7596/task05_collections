package com.babii.stringcontainer;

import java.util.ArrayList;
import java.util.Random;

public class Test {
    private static final Random random = new Random();
    public static long testAdd(ArrayList<String> list, int repeats) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < repeats; i++) {
            list.add(getRandomString());
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static long testAdd(SimpleStringContainer container, int repeats) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < repeats; i++) {
            container.add(getRandomString());
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static long testGet(ArrayList<String> list, int repeats) {
        if(list.isEmpty()){
            System.out.println("list is empty");
            return 0;
        }
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < repeats; i++) {
            list.get(random.nextInt(list.size()));
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static long testGet(SimpleStringContainer container, int repeats) {
        if(container.size() == 0){
            System.out.println("container is empty");
            return 0;
        }
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < repeats; i++) {
            container.get(random.nextInt(container.size()));
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static long testInsert(ArrayList<String> list, int repeats) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < repeats; i++) {
            list.add(random.nextInt(list.size() + 1), getRandomString());
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static long testInsert(SimpleStringContainer container, int repeats) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < repeats; i++) {
            container.add(random.nextInt(container.size() + 1), getRandomString());
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    private static String getRandomString() {
        String result = "";
        for(int i = 0; i < random.nextInt(20); i++){
            result += 48 + random.nextInt(10);
        }
        return result;
    }
}
