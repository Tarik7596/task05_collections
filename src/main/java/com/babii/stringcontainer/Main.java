package com.babii.stringcontainer;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        SimpleStringContainer container = new SimpleStringContainer();

        while (true) {
            System.out.println("------------------------------");
            System.out.println("1 - очистити контейнери");
            System.out.println("2 - створити контейнери з розміром");
            System.out.println("31 - тест вставки в середину");
            System.out.println("32 - тест вставки в кінець");
            System.out.println("33 - тест get");
            System.out.println("------------------------------");
            int choose = 0;
            choose = SCANNER.nextInt();

            switch (choose) {
                case 1:
                    list.clear();
                    container.clear();
                    break;
                case 2:
                    System.out.print("size = ");
                    int size = SCANNER.nextInt();
                    list = new ArrayList<>(size);
                    container = new SimpleStringContainer(size);
                    break;
                case 31: {
                    // TODO: inserting time is too different

                    System.out.println("Testing inserts...");
                    System.out.println("How much inserts to do: ");
                    int repeats = SCANNER.nextInt();
                    System.out.println("ArrayList, time = " + Test.testInsert(list, repeats) + "ms");
                    System.out.println("SimpleStringContainer, time = " + Test.testInsert(container, repeats) + "ms");
                    break;
                }
                case 32: {
                    System.out.println("Testing adding...");
                    System.out.println("How much adding to do: ");
                    int repeats = SCANNER.nextInt();
                    System.out.println("ArrayList, time = " + Test.testAdd(list, repeats) + "ms");
                    System.out.println("SimpleStringContainer, time = " + Test.testAdd(container, repeats) + "ms");
                    break;
                }
                case 33: {
                    System.out.println("Testing getting...");
                    System.out.println("How much getting to do: ");
                    int repeats = SCANNER.nextInt();
                    System.out.println("ArrayList, time = " + Test.testGet(list, repeats) + "ms");
                    System.out.println("SimpleStringContainer, time = " + Test.testGet(container, repeats) + "ms");
                    break;
                }
                case 0:
                    System.out.println("exit...");
                    System.exit(0);
                default:
                    System.out.println("incorrect input");
            }
        }
    }
}
