package com.babii.stringcontainer;

public class SimpleStringContainer {
    private final static int MIN_INITIAL_CAPACITY = 16;

    private int capacity;
    private int size;
    private String[] array;

    private void rangeCheck(int index) {
        if ((index >= size && index != 0) || index < 0)
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    private void increaseCapacity() {
        this.capacity = (int) (capacity * 1.7);

        String[] newArray = new String[capacity];
        for (int i = 0; i < size; i++) {
            newArray[i] = array[i];
        }
        this.array = newArray;
    }

    public SimpleStringContainer() {
        this(MIN_INITIAL_CAPACITY);
    }

    public SimpleStringContainer(int initCapacity) {
        capacity = initCapacity;
        array = new String[capacity];
        size = 0;
    }

    public String get(int index) {
        rangeCheck(index);
        return array[index];
    }

    public void add(String str) {
        if (size == capacity)
            increaseCapacity();
        array[size] = str;
        size++;
    }

    public void add(int index, String str) {
        rangeCheck(index);
        if (size == capacity)
            increaseCapacity();
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size(); i++)
            sb.append((array[i] + ", "));
        sb.append("]");
        return sb.toString();
    }

    public int size() {
        return size;
    }

    public void clear() {
        for (int i = 0; i < size; i++)
            array[i] = null;
        size = 0;
    }
}
